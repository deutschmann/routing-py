"""
Chinese Postman Problem (CPP)
→ visit all edges
"""

import networkx as nx
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from util import plt_graph, total_length

"""
- "gewichtetes Matching-Problem" -> given complete graph, match nodes s.t. complete edge sum is minimal 
-- 1. find nodes with odd degree
-- 2. compute shortest paths between all of them
-- 3. find ideal matching s.t. then all nodes have even degree
-> then we can find a Eulerian path (Eulerkreis)

- take random Eulerian path
- target value (Zielfunktionswert) = sum of all edges + sum of newly added (imaginary) edges
- = optimal route!

"""


def convert_to_graph(l):
    G = nx.Graph()
    G.add_weighted_edges_from(l)
    return G


def eulerize(G):
    F = nx.MultiGraph(G)  # might have multiple edges between nodes

    uneven_nodes = list(filter(lambda x: G.degree[x] % 2 != 0, dict(G.degree).keys()))

    shortest_paths = pd.DataFrame(columns=uneven_nodes, index=uneven_nodes)
    for u in uneven_nodes:
        for v in uneven_nodes:
            shortest_paths[u][v] = nx.algorithms.weighted.dijkstra_path_length(G, u, v, "weight")

    shortest_paths_np = shortest_paths.to_numpy('int') * -1

    M = nx.from_numpy_matrix(shortest_paths_np)
    M = nx.relabel_nodes(M, dict(zip(list(range(len(uneven_nodes))), uneven_nodes)))
    plt_graph(M, "1 CPP matching problem (with inverted weights)")

    matching = nx.max_weight_matching(M, maxcardinality=True)

    for (u, v) in matching:
        w = shortest_paths[u][v]
        print("Adding matching %s → %s (weight=%d)..." % (u, v, w))

        # add dijkstra path
        dijkstra_path = nx.algorithms.weighted.dijkstra_path(G, u, v, "weight")
        i = None
        for j in dijkstra_path:
            if i is None:
                i = j
                continue
            w_ij = G.adj[i][j]["weight"]
            F.add_edge(i, j, weight=w_ij)
            print("| %s → %s (weight=%d)" % (i, j, w_ij))
            i = j

    plt_graph(F, "2 CPP matched graph")

    return F


def solve_cpp(G):
    plt_graph(ex, "0 CPP initial situation")

    F = eulerize(G)

    euler_circuit = list(nx.eulerian_circuit(F))
    result = " → ".join(list(map(lambda x: str(x[0]), euler_circuit))) + " → " + str(euler_circuit[0][0])

    print("Results")
    print("-------")
    print("Eulerian Circuit: " + result)
    print("Total length: " + str(total_length(F)))


if __name__ == '__main__':
    # list of edges in the format (u, v, weight)
    ex = convert_to_graph([
        (1, 4, 1),
        (1, 5, 1),
        (4, 5, 1),
        (5, 2, 1),
        (5, 3, 1),
        (10, 5, 1),
        (10, 3, 1),
        (10, 8, 1),
        (10, 2, 1),
        (2, 8, 1),
        (8, 9, 1),
        (9, 2, 1),
        (9, 6, 1),
        (7, 2, 1),
        (6, 7, 1),
        (1, 7, 1),
        (1, 2, 1),
        (1, 9, 1)
    ])

    solve_cpp(ex)

    print("done")
