import networkx as nx
import matplotlib.pyplot as plt
import numpy as np


def relabel(G):
    """
    Start keys with 1 instead of 0
    :param G:
    :return:
    """
    mapping = dict(zip(G.nodes, list(map(lambda x: x + 1, G.nodes))))
    return nx.relabel_nodes(G, mapping)


def convert_start_to_graph(G, start_tour, relabel_G=True):
    if relabel_G:
        G = relabel(G)
    F = nx.MultiGraph()
    u = None
    for v in start_tour:
        if u is None:
            u = v
        else:
            F.add_edge(u, v, weight=G.adj[u][v]["weight"])
            u = v
    return F


def total_length(G):
    return round(sum(nx.get_edge_attributes(G, "weight").values()), 2)


def plt_graph(G, title, matplotlib=True, graphviz=True):
    if matplotlib:
        pos = nx.spring_layout(G)
        nx.draw_networkx(G, pos=pos)
        labels = nx.get_edge_attributes(G, 'weight')
        labels = {(key[0], key[1]): value for key, value in labels.items()}  # remove third value in key for MultiGraphs
        nx.draw_networkx_edge_labels(G, pos=pos, edge_labels=labels)

        plt.title(title)
        plt.show()

    if graphviz:
        G.graph['edge'] = {'splines': 'curved'}
        G.graph['graph'] = {'scale': '3', 'name': title}

        A = nx.drawing.nx_agraph.to_agraph(G)
        A.layout('dot')

        # set edge labels
        for pair in G.edges:
            edge = A.get_edge(pair[0], pair[1])
            if type(G) is nx.MultiGraph:
                w = G.adj[pair[0]][pair[1]][0]["weight"]
            else:
                w = G.adj[pair[0]][pair[1]]["weight"]
            edge.attr['label'] = str(w) + "  "

        A.draw("plots/" + title + '.png')
