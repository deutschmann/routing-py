"""
Vehicle Routing Problem (VRP)
→ visit all nodes
→ capacity restrictions on on nodes
→ time and distance restrictions on on edges
"""

import networkx as nx
import matplotlib.pyplot as plt
import numpy as np
from util import plt_graph, total_length, relabel, convert_start_to_graph
import sys
from tsp import two_opt
import itertools


def sweep(G, depot, sweep_order, max_distance):
    """
    Applies the Sweep algorithm
    :param G: fully-connected graph
    :param depot: node that is the depot
    :param sweep_order: order by which the nodes should be sweeped
    :param max_distance: longest distance the vehicle can drive
    :return:
    """

    G = relabel(G)

    # Sanity checks
    for e in G.nodes:
        if e not in sweep_order and e != depot:
            raise RuntimeError("Not all nodes in sweep order (and depot)")

    plt_graph(G, "0 Sweep: initial situation", graphviz=False)

    F = nx.MultiGraph()
    all_tours_length = 0
    tours = {}

    u = depot
    cur_tour_length = 0
    cur_tour_id = 1
    tours[cur_tour_id] = [u]

    v_idx = 0
    while v_idx < len(sweep_order):
        v = sweep_order[v_idx]
        total_tour_length = cur_tour_length + G.adj[u][v]["weight"] + G.adj[v][depot]["weight"]
        if total_tour_length <= max_distance:
            print("Adding %d to tour #%d, new length: %f" %
                  (v, cur_tour_id, total_tour_length))
            F.add_edge(u, v, weight=G.adj[u][v]["weight"])
            tours[cur_tour_id].append(v)
            cur_tour_length += G.adj[u][v]["weight"]
            u = v
            v_idx += 1
        else:
            print("Cannot add %d to tour #%d anymore, new length would be: %f" %
                  (v, cur_tour_id, total_tour_length))

            # close previous tour
            v_prev = sweep_order[v_idx - 1]
            F.add_edge(v_prev, depot, weight=G.adj[v_prev][depot]["weight"])
            tours[cur_tour_id].append(depot)
            cur_tour_length += G.adj[v_prev][depot]["weight"]

            assert cur_tour_length <= max_distance, "Implementation error: tour distance must not exceed max distance"

            plt_graph(F, "X Sweep: Tour #%d completed with length %f" %
                      (cur_tour_id, cur_tour_length))

            # prepare for next tour
            u = depot
            print("Tour #%d (%s) finished, total length: %f" %
                  (cur_tour_id, ' → '.join(map(str, tours[cur_tour_id])), cur_tour_length))
            all_tours_length += cur_tour_length
            cur_tour_id += 1
            tours[cur_tour_id] = [depot]
            cur_tour_length = 0

    if u != depot:
        # still have to close the tour
        v_prev = sweep_order[v_idx - 1]
        F.add_edge(v_prev, depot, weight=G.adj[v_prev][depot]["weight"])
        tours[cur_tour_id].append(depot)
        cur_tour_length += G.adj[v_prev][depot]["weight"]
        print("Closing last tour (#%d: %s), total length: %f" %
              (cur_tour_id, ' → '.join(map(str, tours[cur_tour_id])), cur_tour_length))
        all_tours_length += cur_tour_length

    all_tours_length = round(all_tours_length, 2)  # work around float imprecision
    assert all_tours_length == total_length(F), "Lengths of all tours doesn't add up"

    print("Sweep done with total length: %f" % all_tours_length)

    plt_graph(F, "1 Sweep: Final route with total length of all tours: %f" % all_tours_length, graphviz=False)

    return F, tours


def two_opt_to_sweep_tours(G, tours):
    print("Checking if we find improvements for sweep...")
    for tour_id, start in tours.items():
        H = convert_start_to_graph(G, start)
        K = two_opt(G, H, "Sweep tour #%d" % tour_id)
        found_improvement = not nx.is_isomorphic(H, K)
        print("Tour #%d (%s): found%s improvement"
              % (tour_id, ' → '.join(map(str, tours[tour_id])), "" if found_improvement else " no"))


def graph_from_tours(G, tours, depot):
    t = [item for sublist in tours.values() for item in sublist[:-1]]
    t.append(depot)

    return convert_start_to_graph(G, t, relabel_G=False)


def print_tours(G, tours):
    all_tours_length = 0
    for tour_id, tour in tours.items():
        s = ' → '.join(map(str, tour))
        l = total_length(convert_start_to_graph(G, tour, relabel_G=False))
        all_tours_length += l
        print("Tour #%d: %s, length: %f" % (tour_id, s, l))
    return all_tours_length


def savings(G, depot, max_distance):
    G = relabel(G)

    # build trivial solution
    F = nx.MultiGraph()
    tours = {}

    cur_tour_id = 1
    for n in G.nodes:
        if n == depot:
            continue
        else:
            w = G.adj[depot][n]["weight"]
            F.add_edge(depot, n, weight=w)
            F.add_edge(n, depot, weight=w)

            tours[cur_tour_id] = [depot, n, depot]
            cur_tour_id += 1

    plt_graph(F, "0 Savings: initial situation")

    iteration = 1

    start = True
    candidate = None

    while start or candidate is not None:
        print("\nStarting iteration %d..." % iteration)

        start = False
        candidate = None
        candidate_saving = 0

        for (t1_id, t1_tour), (t2_id, t2_tour) in itertools.product(tours.items(), tours.items()):
            if t1_id == t2_id:
                continue
            t1_01 = (t1_tour[1], t1_tour[-2])
            t2_01 = (t2_tour[1],  t2_tour[-2])
            for i_1 in [0, 1]:
                for i_2 in [0, 1]:
                    t1_first = t1_01[i_1]
                    t1_last = t1_01[0 if i_1 == 1 else 1]
                    t2_first = t2_01[i_2]
                    t2_last = t2_01[0 if i_2 == 1 else 1]

                    tour = [depot, t1_first]

                    if len(t1_tour) > 4:
                        tour.extend(t1_tour[2:-2])
                    if len(t1_tour) > 3:
                        tour.append(t1_last)

                    tour.append(t2_first)
                    if len(t2_tour) > 4:
                        tour.extend(t2_tour[2:-2])
                    if len(t2_tour) > 3:
                        tour.append(t2_last)

                    tour.append(depot)

                    tour_G = convert_start_to_graph(G, tour, relabel_G=False)
                    tour_len = total_length(tour_G)
                    if tour_len < max_distance:
                        t1_len = total_length(convert_start_to_graph(G, t1_tour, relabel_G=False))
                        t2_len = total_length(convert_start_to_graph(G, t2_tour, relabel_G=False))
                        saving = (t1_len + t2_len) - tour_len
                        assert saving >= 0
                        if candidate is None or saving > candidate_saving:
                            candidate_saving = saving
                            candidate = (t1_id, t2_id, tour, saving)
                            print("✅ Found better candidate by merging tour #%d with #%d using %s, saving: %f" %
                                  (t1_id, t2_id, ' → '.join(map(str, tour)), saving))
                    else:
                        print("ℹ️ Not possible to merge tour #%d with #%d using %s, %f > %f" %
                              (t1_id, t2_id, ' → '.join(map(str, tour)), tour_len, max_distance))

        # apply saving to data structures
        new_tour_id = cur_tour_id
        cur_tour_id += 1

        if candidate is None:
            print("😌 Iteration %d done. No candidate found" % iteration)
            break
        else:
            del tours[candidate[0]]
            del tours[candidate[1]]
            tours[new_tour_id] = candidate[2]

            F = graph_from_tours(G, tours, depot)
            plt_graph(F, "%d Savings iteration" % iteration)

            print("🎉 Iteration %d done. Best candidate is to merge #%d with #%d using %s with saving %f "
                  "→ creating new tour #%d."
                  % (iteration, candidate[0], candidate[1], ' → '.join(map(str, candidate[2])), candidate[3],
                     new_tour_id))
            print("Current tours (total length: %f):" % total_length(F))
            print_tours(G, tours)

            iteration += 1

    print("Savings done.")
    plt_graph(F, "X Savings final result")

    print("\nFinal results:")
    all_tours_length = print_tours(G, tours)

    assert round(all_tours_length, 2) == total_length(F)
    print("Length of all tours together: %f" % all_tours_length)

    return F


if __name__ == '__main__':
    abb3 = nx.from_numpy_matrix(np.array([
        [0.00, 6.13, 5.45, 6.18, 3.61, 3.63, 5.28, 3.65],
        [6.13, 0.00, 8.86, 6.38, 3.02, 2.50, 9.74, 6.09],
        [5.45, 8.86, 0.00, 3.77, 5.95, 7.00, 1.63, 2.79],
        [6.18, 6.38, 3.77, 0.00, 4.26, 5.49, 5.32, 2.65],
        [3.61, 3.02, 5.95, 4.26, 0.00, 1.24, 6.73, 3.16],
        [3.63, 2.50, 7.00, 5.49, 1.24, 0.00, 7.62, 4.24],
        [5.28, 9.74, 1.63, 5.32, 6.73, 7.62, 0.00, 3.74],
        [3.65, 6.09, 2.79, 2.65, 3.16, 4.24, 3.74, 0.00]
    ]))

    abb3_depot = 8
    abb3_sweep_order = [3, 7, 1, 6, 5, 2, 4]
    abb3_max_distance = 13

    # sweep_graph, sweep_tours = sweep(abb3, abb3_depot, abb3_sweep_order, abb3_max_distance)
    # two_opt_to_sweep_tours(abb3, sweep_tours)

    savings(abb3, abb3_depot, abb3_max_distance)

    print("Done.")
