"""
Travelling Salesman Problem (TSP)
→ visit all nodes
→ no restrictions
"""

import networkx as nx
import matplotlib.pyplot as plt
import numpy as np
from util import plt_graph, total_length, relabel, convert_start_to_graph
import sys


def nearest_neighbour(G, start_node=1):
    """
    Implementation of Nearest-Neighbour heuristic
    :param G: fully-connected start graph with weights
    :param start_node: node to start with
    :return: graph connected using Nearest-Neighbour-Heuristic
    """

    G = relabel(G)

    print("Nearest-neighbour:")

    plt_graph(G, "Start nearest neighbour")

    # 1. Initialize all vertices as unvisited.
    F = nx.Graph()
    visited = []

    # 2. Select an arbitrary vertex, set it as the current vertex u. Mark u as visited.
    u = start_node
    v = None
    visited.append(u)
    F.add_node(u)

    total_weight = 0
    solution_str = str(u) + " → "

    print("Start edge: " + str(u))

    # 5. If all the vertices in the domain are visited, then terminate. Else, go to step 3.
    while len(F.nodes) < len(G.nodes):
        # 3. Find out the shortest edge connecting the current vertex u and an unvisited vertex v.
        adj = dict(G.adj[u])
        # filter already visited nodes
        adj = {k: adj[k] for k in filter(lambda x: x not in visited, adj.keys())}
        v = min(adj.keys(), key=(lambda key: adj[key]["weight"]))

        weight = G.adj[u][v]["weight"]
        print("Adding edge %d → %d with weight %f" % (u, v, weight))
        solution_str += str(v) + " → "

        # 4. Set v as the current vertex u. Mark v as visited.
        F.add_edge(u, v, weight=weight)
        visited.append(v)
        total_weight += weight
        u = v

    # connect last node to start node
    weight = G.adj[v][start_node]["weight"]
    F.add_edge(v, start_node, weight=weight)
    total_weight += weight
    solution_str += str(start_node)

    assert total_weight == total_length(F)

    print("final solution: " + solution_str)
    print("total weight: " + str(total_weight))
    plt_graph(F, "Result nearest neighbour: " + str(total_weight))

    return F, total_weight


def x_insertion(G, start_tour, nearest, detail_plot=True):
    """

    :param G: fully-connected start graph with weights
    :param start_tour: list of start tour
    :param nearest: if True, use Nearest-Insertion, else use Farthest-Insertion
    :return:
    """

    G = relabel(G)

    F = convert_start_to_graph(G, start_tour, relabel_G=False)

    if nearest:
        title = "Nearest insertion"
    else:
        title = "Farthest insertion"
    print("Starting " + title + "...")

    plt_graph(G, "Full graph " + title)
    plt_graph(F, "Start tour " + title)

    remaining = list(filter(lambda x: x not in start_tour, G.nodes))

    i = 1

    while len(F.nodes) < len(G.nodes):
        print("Iteration " + str(i) + ":")

        # 1. finding nearest/farthest selection
        v_candidate = None
        v_candidate_weight = None

        for v in remaining:
            u_candidate = None
            u_candidate_weight = None
            # find min (already contained edge that's closest to candidate v)
            for u in F.nodes:
                weight = G.adj[u][v]["weight"]
                if (u_candidate is None) or \
                        (weight < u_candidate_weight):
                    u_candidate = v
                    u_candidate_weight = weight
            # find overall min/max (depending on nearest/farthest)
            if (v_candidate is None) or \
                    (nearest and u_candidate_weight < v_candidate_weight) or \
                    ((not nearest) and u_candidate_weight > v_candidate_weight):
                v_candidate = u_candidate
                v_candidate_weight = u_candidate_weight
        print("- selected node " + str(v_candidate) + " with weight " + str(v_candidate_weight))

        # 2. finding cheapest insertion
        edge_candidate = None
        edge_candidate_weight = None
        for e in F.edges:
            # create new graph and try out all potential new sub-routes
            E = F.copy()
            E.remove_edge(e[0], e[1])
            E.add_edge(e[0], v_candidate, weight=G.adj[e[0]][v_candidate]["weight"])
            E.add_edge(e[1], v_candidate, weight=G.adj[e[1]][v_candidate]["weight"])
            weight = total_length(E)
            if (edge_candidate is None) or \
                    (weight < edge_candidate_weight):
                edge_candidate = e
                edge_candidate_weight = weight

        print("- inserting on edge " + str(edge_candidate) + " with total tour weight " + str(edge_candidate_weight))

        # 3. performing actual insertion
        F.remove_edge(edge_candidate[0], edge_candidate[1])
        F.add_edge(edge_candidate[0], v_candidate, weight=G.adj[edge_candidate[0]][v_candidate]["weight"])
        F.add_edge(edge_candidate[1], v_candidate, weight=G.adj[edge_candidate[1]][v_candidate]["weight"])

        remaining.remove(v_candidate)

        if detail_plot:
            plt_graph(F, "Iteration " + str(i))

        i += 1

    total = total_length(F)
    plt_graph(F, "Final result " + title + ": " + str(total))
    print("final solution: <better see plot>, " + str(F.edges))
    print("total weight: " + str(total))

    return F, total


def two_opt(G, F, title=""):
    """
    Apply the 2-opt heuristic
    :param G: fully-connected graph
    :param F: graph with initial route
    :return: graph optimised by 2-opt
    """

    print("Starting 2-Opt '%s'" % title)

    G = relabel(G)

    H = F.copy()
    old_length = total_length(H)

    print("Starting length = " + str(old_length))

    iteration = 1
    candidate_length = None

    while candidate_length is None or candidate_length < old_length:
        print("Iteration %d:" % iteration)

        if candidate_length is not None:
            old_length = candidate_length

        candidate = None
        candidate_length = old_length

        for a in list(H.edges):
            for b in list(H.edges):
                new_length = None

                # skip direct neighbour edges
                if any(x in a for x in b):
                    continue

                for i in [0, 1]:
                    X = H.copy()

                    X.remove_edge(a[0], a[1])
                    X.remove_edge(b[0], b[1])
                    j = 1 if i is 0 else 0

                    u_1 = a[0]
                    v_1 = b[i]
                    u_2 = a[1]
                    v_2 = b[j]

                    X.add_edge(u_1, v_1, weight=G.adj[u_1][v_1]["weight"])
                    X.add_edge(u_2, v_2, weight=G.adj[u_2][v_2]["weight"])

                    if not nx.is_connected(X):
                        continue
                    else:
                        X_length = total_length(X)
                        if X_length < candidate_length:
                            candidate = X
                            candidate_length = X_length
                            plt_graph(X, "Improvement found in iteration " + str(iteration), graphviz=False)
                            res_str = "✅ Good"
                        else:
                            res_str = "ℹ️ Bad"
                        print(res_str + " option found: Removing %s and %s, adding (%d, %d) and (%d, %d) to obtain new length %s"
                                  % (str(a), str(b), u_1, v_1, u_2, v_2, str(X_length)))

        if candidate is not None:
            print("Deciding for option with length " + str(candidate_length))
            H = candidate

        iteration += 1

    print("No more improvements found")

    plt_graph(H, "2-Opt '%s' final result with total length %f" % (title, old_length))

    print("2-Opt '%s' done" % title)

    return H


if __name__ == '__main__':
    abb3 = nx.from_numpy_matrix(np.array([
        [0.00, 6.13, 5.45, 6.18, 3.61, 3.63, 5.28, 3.65],
        [6.13, 0.00, 8.86, 6.38, 3.02, 2.50, 9.74, 6.09],
        [5.45, 8.86, 0.00, 3.77, 5.95, 7.00, 1.63, 2.79],
        [6.18, 6.38, 3.77, 0.00, 4.26, 5.49, 5.32, 2.65],
        [3.61, 3.02, 5.95, 4.26, 0.00, 1.24, 6.73, 3.16],
        [3.63, 2.50, 7.00, 5.49, 1.24, 0.00, 7.62, 4.24],
        [5.28, 9.74, 1.63, 5.32, 6.73, 7.62, 0.00, 3.74],
        [3.65, 6.09, 2.79, 2.65, 3.16, 4.24, 3.74, 0.00]
    ]))

    ex_start_tour = [1, 8, 7, 1]
    F, _ = x_insertion(abb3, ex_start_tour, True)

    # F, _ = nearest_neighbour(abb3)

    H = two_opt(abb3, F)



    # ------------------------

    # hw_1_nearest_neighbour = nx.from_numpy_matrix(np.array([
    #     [0, 8, 4, 73, 23, 7, 75, 66],
    #     [8, 0, 79, 35, 45, 72, 61, 68],
    #     [4, 79, 0, 69, 40, 70, 28, 36],
    #     [73, 35, 69, 0, 2, 80, 78, 11],
    #     [23, 45, 40, 2, 0, 14, 53, 74],
    #     [7, 72, 70, 80, 14, 0, 49, 30],
    #     [75, 61, 28, 78, 53, 49, 0, 43],
    #     [66, 68, 36, 11, 74, 30, 43, 0]
    # ]))
    #
    # nearest_neighbour(hw_1_nearest_neighbour)

    # ------------------------

    # hw_2_nearest_insertion_G = nx.from_numpy_matrix(np.array([
    #     [0, 22, 5, 8, 17],
    #     [22, 0, 23, 28, 19],
    #     [5, 23, 0, 4, 9],
    #     [8, 28, 4, 0, 14],
    #     [17, 19, 9, 14, 0]
    # ]))
    #
    # hw_2_nearest_insertion_start = [1, 2, 3, 1]
    #
    # x_insertion(hw_2_nearest_insertion_G, hw_2_nearest_insertion_start, True)

    # ------------------------

    # hw_3_farthest_insertion_G = nx.from_numpy_matrix(np.array([
    #     [0, 31, 14, 2, 43, 6],
    #     [31, 0, 40, 37, 24, 25],
    #     [14, 40, 0, 9, 12, 26],
    #     [2, 37, 9, 0, 7, 4],
    #     [43, 24, 12, 7, 0, 29],
    #     [6, 25, 26, 4, 29, 0]
    # ]))
    #
    # hw_3_farthest_insertion_start = [1, 2, 3, 1]
    #
    # F, _ = x_insertion(hw_3_farthest_insertion_G, hw_3_farthest_insertion_start, False)

    print("\nDone.")
